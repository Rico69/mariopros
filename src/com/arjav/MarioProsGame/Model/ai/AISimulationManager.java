package com.arjav.MarioProsGame.Model.ai;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Random;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RefineryUtilities;
import com.arjav.MarioProsGame.Controller.Game;
import com.arjav.MarioProsGame.Controller.Game.AIState;

import javax.swing.*;

public class AISimulationManager implements Runnable{
	private XYSeriesCollection dataset;
	private XYSeries fitnessSeries;
	private String title, logFile;
	private int currentGenMaxFitness, lastGenMaxFitness;
	private int currentGen, currentSpecies;
	private ArrayList<double[][]> lastGenBestInputWeights, currentGenBestInputWeights;
	
	private final static int MIN_SPECIES_PER_GEN = 3, MAX_SPECIES_PER_GEN = 3;//20,1 ,10 200
	private final static int ACCEPTABLE_NO_PROGRESS_TIME_MILLIS = 3000;
	public final static int LEVEL_PASS_BONUS = 1000;
	public final static int GAME_PASS_BONUS = 3000;
	private int Def_Layer1 = 1000,Def_Layer2 =500,Def_Layer3 = 100,Def_Layer4 =50,Def_Layer5 =10;
	public int curfit;
	private static long last = 0;
	PriorityQueue<ArrayList<double[][]>> BestWeight;

	private Font genFont, speciesFont, fitnessFont;
	
	public AISimulationManager(String title, String logFile) {
		this.title = title;
		this.logFile = logFile;
		currentGenMaxFitness = lastGenMaxFitness = 0;
		//getting a new neural network
		genFont = speciesFont = fitnessFont = new Font("Serif", Font.BOLD, 12);
		BestWeight =  new PriorityQueue<>((a,b) ->Integer.compare(currentGenMaxFitness,lastGenMaxFitness));;
		currentGen = currentSpecies = 0;
		lastGenBestInputWeights = new ArrayList<double[][]>();//creating a temporary network
		lastGenBestInputWeights.add(new double[Def_Layer1][AIPlayer.N_INPUT_NEURONS]);
		lastGenBestInputWeights.add(new double[Def_Layer2][Def_Layer1]);
		lastGenBestInputWeights.add(new double[Def_Layer3][Def_Layer2]);
		lastGenBestInputWeights.add(new double[Def_Layer4][Def_Layer3]);
		lastGenBestInputWeights.add(new double[Def_Layer5][Def_Layer4]);
		lastGenBestInputWeights.add(new double[AIPlayer.N_KEYS_TO_PRESS][Def_Layer5]);
		Random random = new Random(System.nanoTime());
		for(double[][] layer : lastGenBestInputWeights) {
			for(int i = 0; i < layer.length; i++) {//starting the model with random inputs
				for(int j = 0; j < layer[0].length; j++) {
					layer[i][j] = (random.nextDouble() - 0.5) * 2 * 5; // generates weights of +-5.0
				}
			}
		}
		//creating the graph
		fitnessSeries = new XYSeries("Fitness");
		dataset = new XYSeriesCollection(fitnessSeries);
		//creating the best network also a temporary
		currentGenBestInputWeights = new ArrayList<double[][]>();
		currentGenBestInputWeights.add(new double[Def_Layer1][AIPlayer.N_INPUT_NEURONS]);
		currentGenBestInputWeights.add(new double[Def_Layer2][Def_Layer1]);
		currentGenBestInputWeights.add(new double[Def_Layer3][Def_Layer2]);
		currentGenBestInputWeights.add(new double[Def_Layer4][Def_Layer3]);
		currentGenBestInputWeights.add(new double[Def_Layer5][Def_Layer4]);
		currentGenBestInputWeights.add(new double[AIPlayer.N_KEYS_TO_PRESS][Def_Layer5]);
		BestWeight.offer(currentGenBestInputWeights);
		int k = 0;
		for(double[][] layer : lastGenBestInputWeights) {
			for(int i = 0; i < layer.length; i++) {
				for(int j = 0; j < layer[0].length; j++) {//adding the random into it too
					layer[i][j] = lastGenBestInputWeights.get(k)[i][j];
				}
			}
			k++;
		}
	}

	/**
	 * a graph char for the generations over the fittnes for each generation and its species
	 */
	@Override
	public void run() {
		startSim();
		JFreeChart xylineChart = ChartFactory.createXYLineChart(
				"Fitness over Generations",
				"Generation",
				"Fitness",
				dataset,
				PlotOrientation.VERTICAL,
				true, true, false);

		ChartPanel chartPanel = new ChartPanel(xylineChart);
		chartPanel.setPreferredSize(new java.awt.Dimension(80, 60));
		JFrame chartFrame = new JFrame("Simulation Results");
		chartFrame.setContentPane(chartPanel);
		chartFrame.pack();
		RefineryUtilities.centerFrameOnScreen(chartFrame);
		chartFrame.setVisible(true);
	}

	/**
	 * the function starts the training with the random  generated network and proggress for each generation
	 */
	public void startSim() {
		last = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		int x=0;
		while(x++ != 5) {//while the generations isnt finished
			currentGen++;
			boolean improvementMade = false;//each generation
			for(int i = 0; (i < MIN_SPECIES_PER_GEN || !improvementMade) && i < MAX_SPECIES_PER_GEN; i++) {
				currentSpecies++;//start game for each species
				BestWeight.offer(startAndFinishGame(evolveInputWeights(lastGenBestInputWeights)));//inputing the current best weight of spiecies into the priority queue
				currentGenBestInputWeights = startAndFinishGame(evolveInputWeights(lastGenBestInputWeights));
				if(currentGenMaxFitness > lastGenMaxFitness) improvementMade = true;
				System.gc();//if this spiecis is better than the last one
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				printStats();//printing out the graph for the generation
			}
			lastGenMaxFitness = currentGenMaxFitness;
			lastGenBestInputWeights = currentGenBestInputWeights;
			//createAndShowChart();
			logNetwork();
			System.out.println("Highest fitness in this generation: " + currentGenMaxFitness);
			currentSpecies = 0;
		}
		//createAndShowChart();
	}

	/**
	 *
	 * @param inputWeights
	 * @return the best specie out of the current generation
	 */
	private ArrayList<double[][]> startAndFinishGame(ArrayList<double[][]> inputWeights) {
		Game game = new Game(title, AIState.simulation, inputWeights);
		game.addHUDString("Generation: " + currentGen, 50, 200, Color.white, null, genFont);
		game.addHUDString("Species: " + currentSpecies, 50, 250, Color.white, null, speciesFont);
		Game.st = Game.State.start;
		game.start();
		while(!game.readyForAction());
		int lastFitness = AIUtils.getFitness(game);
		int maxFitnessAchievedAtLevel = lastFitness;
		int fitnessHUDID = game.addHUDString("Fitness: " + lastFitness, 50, 300, Color.white, null, fitnessFont);
		long lastTime = 0;
		boolean flag = false;
		
		int[] fitnessAtLevel = {0, 0, 0};
		int lastLevel = 1;
		while(game.isRunning()) {
			if(game.readyForAction() && !game.flagBeingDropped) {//go through the game until the flag dop
				fitnessAtLevel[game.currentLevel()-1] = AIUtils.getFitnessAtCurrentLevel(game);//analayze the current fitness
				if(game.currentLevel() != lastLevel) {
					lastLevel = game.currentLevel();//check if the game is the last level
					maxFitnessAchievedAtLevel = fitnessAtLevel[game.currentLevel()-1];
				}
				//checking for best fittnes for each level
				if(fitnessAtLevel[game.currentLevel()-1] > maxFitnessAchievedAtLevel) maxFitnessAchievedAtLevel = fitnessAtLevel[game.currentLevel()-1];
				int fitness = fitnessAtLevel[0] + fitnessAtLevel[1] + fitnessAtLevel[2];
				game.setHUDString(fitnessHUDID, "Fitness: " + fitness);
				if(fitnessAtLevel[game.currentLevel()-1] < maxFitnessAchievedAtLevel || fitness <= lastFitness) {
					if(flag) {//if the player is stuck or did something wrong
						if((System.currentTimeMillis() - lastTime >= ACCEPTABLE_NO_PROGRESS_TIME_MILLIS / Game.AMOUNT_OF_TICKS * Game.DEFAULT_AMOUNT_OF_TICKS)) {
							game.endGame(2); // timeout
						}
					} else {
						flag = true;
						lastTime = System.currentTimeMillis();
					}
					
				} else flag = false;
				lastFitness = fitness;
			}
		}
		int thisGamesFitness = AIUtils.getFitness(game);
		System.out.println("level 0 fit:"+fitnessAtLevel[0] + " level 1 fit:" + fitnessAtLevel[1] + " level 2 fit:" + fitnessAtLevel[2]);
		if(thisGamesFitness > currentGenMaxFitness) {
			currentGenMaxFitness = thisGamesFitness;
			return inputWeights;
		}
		return currentGenBestInputWeights;
	}

	/**
	 * for each network evolve the weights of the neuron
	 * @param inputWeights
	 * @return
	 */
	private ArrayList<double[][]> evolveInputWeights(ArrayList<double[][]> inputWeights) {
		for(double[][] layerInputWeights : inputWeights) {
			Random random = new Random(System.nanoTime());
			int neuronIndex = (int)(random.nextInt(layerInputWeights.length));
			int weightIndex = (int)(random.nextInt(layerInputWeights[0].length));
			// layerInputWeights[0] because number of inputs is same for all neurons in a layer
			layerInputWeights[neuronIndex][weightIndex] = (random.nextDouble() - 0.5) * 2 * 5; // generates weights other than numbers between -5.0 and 5.0
		}
		return inputWeights;
	}
	
	private void logNetwork() {
		String fileName = logFile.substring(0, logFile.length()-3) + currentGen + logFile.substring(logFile.length()-3, logFile.length());
		System.out.println("Logging");
		new AIUtils().writeNetworkToFile(fileName, BestWeight.poll());//getting the best network from the priority queue
		System.gc();
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Logged");
		
	}
	
	private void printStats() {
		int mb = 1024 * 1024;
		long memInUse = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		if(memInUse > last) System.out.println("stats: "+(memInUse-last)/mb);
		last = memInUse;
	}
	private void createAndShowChart() {
		JFreeChart xylineChart = ChartFactory.createXYLineChart(
				"Fitness over Generations",
				"Generation",
				"Fitness",
				dataset,
				PlotOrientation.VERTICAL,
				true, true, false);

		ChartPanel chartPanel = new ChartPanel(xylineChart);
		chartPanel.setPreferredSize(new java.awt.Dimension(800, 600));
		JFrame chartFrame = new JFrame("Simulation Results");
		chartFrame.setContentPane(chartPanel);
		chartFrame.pack();
		RefineryUtilities.centerFrameOnScreen(chartFrame);
		chartFrame.setVisible(true);
		chartFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // Ensure the application exits when the frame is closed
	}
}
