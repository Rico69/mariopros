package com.arjav.MarioProsGame.View.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;

import com.arjav.MarioProsGame.Controller.Game;

public class MyKeyListener implements KeyListener{

	private Game game;
	HashMap<Integer, Runnable> keyMappings;

	public MyKeyListener(Game game) {
		keyMappings = new HashMap<>();
		keyMappings.put(KeyEvent.VK_ESCAPE, () -> Game.hideWindow(game));
		keyMappings.put(KeyEvent.VK_1, () -> Game.AMOUNT_OF_TICKS = 60);
		keyMappings.put(KeyEvent.VK_2, () -> Game.AMOUNT_OF_TICKS = 120);
		keyMappings.put(KeyEvent.VK_3, () -> Game.AMOUNT_OF_TICKS = 180);
		keyMappings.put(KeyEvent.VK_4, () -> Game.AMOUNT_OF_TICKS = 240);
		this.game = game;
	}

	// method called when a key is pressed
	@Override
	public void keyPressed(KeyEvent e) {
		int code=  e.getKeyCode();
		if(game.isPlayingAI()) {
			try {
			keyMappings.get(code).run();
			} catch (Exception ex) {System.out.println(ex);}
			//case KeyEvent.VK_ESCAPE:
			//	Game.hideWindow(game);
			//	break;
			//case KeyEvent.VK_1:
			//	Game.AMOUNT_OF_TICKS = 60;
			//	break;
			//case KeyEvent.VK_2:
			//	Game.AMOUNT_OF_TICKS = 120;
			//	break;
			//case KeyEvent.VK_3:
			//	Game.AMOUNT_OF_TICKS = 180;
			//	break;
			//case KeyEvent.VK_4:
			//	Game.AMOUNT_OF_TICKS = 240;
			//	break;
			//}
		}
		else game.getKeyManager().keyPress(e.getKeyCode());

		if(code == KeyEvent.VK_0){game.getSounds().toggleMusic();}
	}


		
	// method called when the key is released
	@Override
	public void keyReleased(KeyEvent e) {
		if(game.isPlayingAI()) return;
		game.getKeyManager().keyRelease(e.getKeyCode());
		// then the game.getPlayer() should stop moving along the x axis. Therefore, to allow decrementation of it's accelX, we set game.getPlayer()'s keyPressed value to true
	}
	
	// method not using
	@Override
	public void keyTyped(KeyEvent e) {

	}

}
